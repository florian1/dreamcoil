# Dreamcoil
### PhP framework for smooth people
## Quickstart

1.  [Get it!](https://github.com/xolf/dreamcoil/archive/master.zip)
2.  Unzip the files in a directory
3.  Start using Dreamcoil for your projects
4.  Read the [wiki](https://github.com/xolf/dreamcoil/wiki)

Or install it with Composer. "xolf/dreamcoil".

After downloading these files remove the README.md.

```php
if(ROUTE == '/'){

	include(getView('welcome.hello'));

}
```


[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/xolf/dreamcoil/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

